package com.nanda.bsit.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nanda.bsit.R
import com.nanda.bsit.databinding.ItemTransactionBinding
import com.nanda.bsit.model.ContactResponse
import com.nanda.bsit.model.TransactionResponse

class TransactionAdapter() : RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    private var data: MutableList<TransactionResponse> = mutableListOf()

    fun setData(newData: MutableList<TransactionResponse>) {
        data = newData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TransactionViewHolder(
        ItemTransactionBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bindingView(data[position])

    }



    inner class TransactionViewHolder(val binding: ItemTransactionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindingView(item: TransactionResponse) {
            binding.ccTvTitle.text = item.name
            binding.ccTvSubtitle.text = item.metodeTrf
            binding.ccTvPrice.text = item.nominalSaldo
            if (item.flagDebitCredit == 1) {
                binding.ccTvPrice.setTextColor(
                    binding.root.context.resources.getColor(R.color.red)
                )
            } else {
                binding.ccTvPrice.setTextColor(
                    binding.root.context.resources.getColor(R.color.green)
                )
            }

            Glide
                .with(binding.root.context)
                .load(item.imageUrl)
                .centerCrop()
                .into(binding.ccImage);
        }

    }

    override fun getItemCount() = data.size

}