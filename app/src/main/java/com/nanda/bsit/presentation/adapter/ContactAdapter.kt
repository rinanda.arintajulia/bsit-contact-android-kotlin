package com.nanda.bsit.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nanda.bsit.databinding.ContactItemBinding
import com.nanda.bsit.databinding.ItemTransactionBinding
import com.nanda.bsit.model.ContactResponse

class ContactAdapter(

) : RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {

    inner class ContactViewHolder(private val binding: ContactItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

                fun bindingView(item: ContactResponse) {
                    binding.ccTvTitle.text = item.name
                    binding.ccPhone.text = item.noTelp
                    binding.ccCall.setOnClickListener { onClickPhone.invoke(item) }
                }
            }

    private var data: MutableList<ContactResponse> = mutableListOf()
    private var onClickPhone: (ContactResponse) -> Unit = {}
    fun onClickIconCall(clickContact: (ContactResponse)-> Unit) {
        onClickPhone = clickContact
    }

    fun setData(newData: MutableList<ContactResponse>) {
        data = newData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ContactViewHolder(
        ContactItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ContactAdapter.ContactViewHolder, position: Int) {
        holder.bindingView(data[position])
    }
}