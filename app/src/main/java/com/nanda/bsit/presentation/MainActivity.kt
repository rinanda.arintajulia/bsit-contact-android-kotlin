package com.nanda.bsit.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import com.google.android.material.tabs.TabLayoutMediator
import com.nanda.bsit.databinding.ActivityMainBinding
import com.nanda.bsit.model.ContactResponse
import com.nanda.bsit.model.TransactionResponse
import com.nanda.bsit.presentation.adapter.BsitFragmentPagerAdapter
import com.nanda.bsit.presentation.adapter.TransactionAdapter
import com.nanda.bsit.presentation.fragment.ContactFragment
import com.nanda.bsit.presentation.fragment.ProfileFragment
import com.nanda.bsit.presentation.fragment.TransactionFragment
import com.nanda.bsit.presentation.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val mainViewModel by viewModels<MainViewModel>()
    private lateinit var binding: ActivityMainBinding

    private lateinit var bsitFragmentPagerAdapter: BsitFragmentPagerAdapter

//    private val transactionAdapter = TransactionAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpViewPager(listOf())
        setUpTabLayout()
        observeViewModel()
        mainViewModel.getTransaction()
        mainViewModel.getContact()
    }

//    private fun setData(dataTransaction: List<TransactionResponse>?, dataContact: List<ContactResponse>) {
//        val bundle = listOf(
//            dataContact.let { ContactFragment.createInstance(it) }
//        )
//        setUpViewPager(bundle)
//    }
//
//    private fun setViewAdapter(){
//        val contactFragment = ContactFragment()
//        val transactionAdapter = TransactionFragment()
//        val profileAdapter = ProfileFragment()
//    }

    private fun setUpViewPager(bundle: List<Bundle>) {
        val viewPager = binding.vpContainer
        viewPager.adapter = BsitFragmentPagerAdapter(this, bundle)

    }

    private fun setUpTabLayout() {
        val tabLayout = binding.tabsFragment
        val viewPager = binding.vpContainer
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when(position) {
                0 -> tab.text = "Transaction"
                1 -> tab.text = "Contact"
                2 -> tab.text = "Profile"
            }
        }.attach()
    }

    private fun observeViewModel() {
        mainViewModel.transaction.observe(this) {
//            setDataForTransaction(it)
        }

        mainViewModel.errorMessage.observe(this) {
            Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
        }
        mainViewModel.showLoading.observe(this) { isShow ->
            if (isShow) showLoading() else hideLoading()
        }
    }


    private fun showLoading() {
        binding.cmpLoading.root.visibility = View.VISIBLE
//        binding.rvListTransaction.visibility = View.GONE
    }

    private fun hideLoading() {
        binding.cmpLoading.root.visibility = View.GONE
//        binding.rvListTransaction.visibility = View.VISIBLE

    }
}