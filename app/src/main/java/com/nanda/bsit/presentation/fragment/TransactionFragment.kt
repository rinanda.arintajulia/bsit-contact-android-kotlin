package com.nanda.bsit.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.nanda.bsit.R
import com.nanda.bsit.databinding.FragmentTransactionBinding
import com.nanda.bsit.model.ContactResponse
import com.nanda.bsit.model.ProfileResponse
import com.nanda.bsit.model.TransactionResponse
import com.nanda.bsit.presentation.adapter.ContactAdapter
import com.nanda.bsit.presentation.adapter.TransactionAdapter
import com.nanda.bsit.presentation.fragment.viewmodel.ProfileViewModel
import com.nanda.bsit.presentation.fragment.viewmodel.TransactionViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TransactionFragment : Fragment() {

    private var _binding: FragmentTransactionBinding? = null
    private val binding get() = _binding

    private val viewModel : TransactionViewModel by viewModels()

    private var transactionAdapter = TransactionAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTransactionBinding.inflate(inflater, container, false)

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeViewModel()
        viewModel.getTransaction()
    }

    private fun observeViewModel() {
        viewModel.transaction.observe(viewLifecycleOwner) { transactionData ->
            if (transactionData != null) {
                setData(transactionData)
            }
        }

    }

    private fun setData(data: List<TransactionResponse>) {
        transactionAdapter.setData(data.toMutableList())

        binding?.rvTransaction?.adapter = transactionAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}