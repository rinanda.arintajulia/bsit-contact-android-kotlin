package com.nanda.bsit.domain.usecase

import com.nanda.bsit.domain.repository.Repository
import com.nanda.bsit.model.ProfileResponse
import retrofit2.Response
import javax.inject.Inject

class GetProfileUseCase @Inject constructor(
    private val repository: Repository
) {

    suspend fun getProfile() : Response<ProfileResponse> {
        return repository.getProfile()
    }
}