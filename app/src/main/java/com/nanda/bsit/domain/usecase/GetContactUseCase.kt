package com.nanda.bsit.domain.usecase

import com.nanda.bsit.domain.repository.Repository
import com.nanda.bsit.model.ContactResponse
import com.nanda.bsit.model.TransactionResponse
import retrofit2.Response
import javax.inject.Inject

class GetContactUseCase @Inject constructor(
    private val repository: Repository
) {

    suspend fun getContact() : Response<List<ContactResponse>> {
        return repository.getContact()
    }
}