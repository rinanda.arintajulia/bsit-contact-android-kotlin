package com.nanda.bsit.domain

import com.nanda.bsit.model.ContactResponse
import com.nanda.bsit.model.ProfileResponse
import com.nanda.bsit.model.TransactionResponse
import retrofit2.Response
import retrofit2.http.GET

interface Service {

    @GET("transaction")
    suspend fun getTransaction() : Response<List<TransactionResponse>>

    @GET("contact")
    suspend fun getContact() : Response<List<ContactResponse>>

    @GET("profile")
    suspend fun getProfile() : Response<ProfileResponse>
}