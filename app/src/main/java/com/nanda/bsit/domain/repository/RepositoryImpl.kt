package com.nanda.bsit.domain.repository

import com.nanda.bsit.domain.RemoteDataSource
import com.nanda.bsit.model.ContactResponse
import com.nanda.bsit.model.ProfileResponse
import com.nanda.bsit.model.TransactionResponse
import retrofit2.Response
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : Repository {

    override suspend fun getTransaction(): Response<List<TransactionResponse>> {
        return remoteDataSource.getTransaction()
    }

    override suspend fun getContact(): Response<List<ContactResponse>> =
        remoteDataSource.getContact()

    override suspend fun getProfile(): Response<ProfileResponse> = remoteDataSource.getProfile()

}
