package com.nanda.bsit.domain.repository

import com.nanda.bsit.model.ContactResponse
import com.nanda.bsit.model.ProfileResponse
import com.nanda.bsit.model.TransactionResponse
import retrofit2.Response

interface Repository {
    suspend fun getTransaction() : Response<List<TransactionResponse>>
    suspend fun getContact() : Response<List<ContactResponse>>
    suspend fun getProfile() : Response<ProfileResponse>
}