package com.nanda.bsit.model


import com.google.gson.annotations.SerializedName

data class ProfileResponse(
    @SerializedName("name")
    var name: String?,
    @SerializedName("joined_date")
    var joinedDate: String?,
    @SerializedName("image_url")
    var imageUrl: String?,
    @SerializedName("no_telp")
    var noTelp: String?,
    @SerializedName("status")
    var status: Int?,
    @SerializedName("lat")
    var lat: Double?,
    @SerializedName("lng")
    var lng: Double?
)